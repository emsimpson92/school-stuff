age <- as.integer(readline(prompt = "enter your age"))
name <- readline(prompt = "enter your name")
print(age)
print(name)

twentyfifty <- 20:50
twentysixty <- 20:60
twentysixtymean <- mean(twentysixty)
fiftyoneninetyone <- 51:91
fiftyoneninetyonesum <- sum(fiftyoneninetyone)

randoms <- sample(-50:50, 10)

print(tolower(letters[1:10]))
print(toupper(letters[16:26]))
print(toupper(letters[24:26]))

sequence <- 1:100
for(val in sequence)
{
    if(val %% 3 == 0 && val %% 5 == 0)
    {
        print("FIZZBUZZ")
    } else if(val %% 3 == 0)
    {
        print("FIZZ")
    } else if(val %% 5 == 0)
    {
        print("BUZZ")
    }
    else
    {
        print(val)
    }
}