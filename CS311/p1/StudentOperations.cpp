#include "./Student.hpp"
#include <vector>
#include <iostream>
#include <fstream>
#include <string>

using namespace std;

class StudentOperations
{
    private:
        
        vector<Student> students;

        int FindStudentPosition(string studentId)
        {
            for(int i = 0; i < students.size(); i++)
            {
                if(students[i].GetId() == studentId)
                {
                    return i;
                }
            }
            
            return -1;
        }
        
    public:

        StudentOperations() { }

        void AddStudent()
        {
            char selection;
            string name, studentId;
            int age, year;
            float gpa;

            do {
                cout << "\n\nEnter student name: ";
                cin >> name;
                cout << "\nEnter student Id: ";
                cin >> studentId;
                cout << "\nEnter student age: ";
                cin >> age;
                cout << "\nEnter student year: ";
                cin >> year;
                cout << "\nEnter student gpa: ";
                cin >> gpa;
                
                Student student = Student(name, studentId, age, year, gpa);
                students.push_back(student);

                cout << "\nEnter another student? (y / n) ";
                cin >> selection;

            } while(tolower(selection) != 'n');
        }

        void RemoveStudent()
        {
            string studentId;
            int position;
            cout << "\n\nEnter student Id: ";
            cin >> studentId;

            position = FindStudentPosition(studentId);
            if(position != -1)
            {
                students.erase(students.begin() + position);
            }
            else
            {
                cout << "\nStudent not found.\n";
            }
        }

        void ReturnStudent()
        {
            string studentId;
            int position;
            cout << "\n\nEnter student Id: ";
            cin >> studentId;

            position = FindStudentPosition(studentId);
            if(position != -1)
            {
                cout << "\nName: " << students[position].GetName() << "\n";
                cout << "Id: " << students[position].GetId() << "\n";
                cout << "Age: " << students[position].GetAge() << "\n";
                cout << "Year: " << students[position].GetYear() << "\n";
                cout << "GPA: " << students[position].GetGPA() << "\n";
            }
            else
            {
                cout << "\nStudent not found.\n";
            }
            
        }

        void ReturnAllStudents()
        {
            for(int i = 0; i < students.size(); i++)
            {
                cout << "\nName: " << students[i].GetName() << "\n";
                cout << "Id: " << students[i].GetId() << "\n";
                cout << "Age: " << students[i].GetAge() << "\n";
                cout << "Year: " << students[i].GetYear() << "\n";
                cout << "GPA: " << students[i].GetGPA() << "\n";
                cout << "\n";
            }
        }

        void ExportStudents()
        {
            ofstream file("students.txt");

            for(int i = 0; i < students.size(); i++)
            {
                file << "\nName: " << students[i].GetName() << "\n";
                file << "Id: " << students[i].GetId() << "\n";
                file << "Age: " << students[i].GetAge() << "\n";
                file << "Year: " << students[i].GetYear() << "\n";
                file << "GPA: " << students[i].GetGPA() << "\n";
                file << "\n";
            }

            file.close();
        }
};