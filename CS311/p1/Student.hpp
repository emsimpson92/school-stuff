#include <string>

using namespace std;

class Student 
{
    private:

        string name;
        string studentId;
        int age;
        int year;
        float GPA;

    public:

        Student(string name, string studentId, int age, int year, float GPA)
        {
            this->name = name;
            this->studentId = studentId;
            this->age = age;
            this->year = year;
            this->GPA = GPA;
        }

        string GetId()
        {
            return studentId;
        }

        string GetName()
        {
            return name;
        }

        int GetAge()
        {
            return age;
        }

        int GetYear()
        {
            return year;
        }

        float GetGPA()
        {
            return GPA;
        }
};