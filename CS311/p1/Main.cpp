#include <iostream>
#include "./StudentOperations.cpp"

using namespace std;

int main()
{
    int menuSelection = -1;
    StudentOperations operations = StudentOperations();

    while(menuSelection != 6)
    {
        cout << "\nDatabase Main Menu:\n\n";
        cout << "1. Add New Student\n";
        cout << "2. Remove Student\n";
        cout << "3. Search Student Record\n";
        cout << "4. List All Students\n";
        cout << "5. Save Student Records to File\n";
        cout << "6. Exit\n\n";
        cout << "Enter Choice: ";

        cin >> menuSelection;

        switch(menuSelection)
        {
            case 1:
                operations.AddStudent();
                break;

            case 2:
                operations.RemoveStudent();
                break;

            case 3:
                operations.ReturnStudent();
                break;

            case 4:
                operations.ReturnAllStudents();
                break;

            case 5:
                operations.ExportStudents();
                break;

            default:
                break;
        }
    }

    return 0;
}