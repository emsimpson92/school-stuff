#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <sstream>
#include "./ByteCodeReader.cpp"

using namespace std;

void ProcessCommand(ByteCodeReader* interpreter, string line)
{
    vector<string> elements;
    istringstream iss(line);
    for(line; iss >> line;)
    {
        elements.push_back(line);
    }

    string first = elements.front();
    if(first == "PUSHX")
    {
        int args = stoi(elements.back());
        interpreter->PushX(args);
    }
    else if(first == "POPX")
    {
        interpreter->PopX();
    }
    else if(first == "ADD")
    {
        interpreter->Add();
    }
    else if(first == "SUB")
    {
        interpreter->Sub();
    }
    else if(first == "MULT")
    {
        interpreter->Mult();
    }
    else if(first == "DIV")
    {
        interpreter->Div();
    }
    else if(first == "DECL")
    {
        interpreter->Decl();
    }
    else if(first == "INCL")
    {
        interpreter->Incl();
    }
    else if(first == "DUP")
    {
        interpreter->Dup();
    }
    else if(first == "SWAP")
    {
        interpreter->Swap();
    }
    else if(first == "PRINT")
    {
        cout << interpreter->Print();
    }
    else if(first == "EXIT")
    {
        interpreter->Exit();
    }
    else if(first == "SHR")
    {
        int args = stoi(elements.back());
        interpreter->ShrX(args);
    }
    else if(first == "SHL")
    {
        int args = stoi(elements.back());
        interpreter->ShlX(args);
    }
    else if(first == "AND")
    {
        interpreter->And();
    }
    else if(first == "OR")
    {
        interpreter->Or();
    }
    else if(first == "XOR")
    {
        interpreter->Xor();
    }
}

int main()
{
    Stack stack = Stack();
    ByteCodeReader interpreter = ByteCodeReader(&stack);
    ifstream file("./sample_bytecode.asm");
    string line;

    while(getline(file, line))
    {
        ProcessCommand(&interpreter, line);
    }

    file.close();

    return 0;
}