#include <vector>

using namespace std;

class Stack
{
    private:

    int top;
    vector<int> values;
    int count;

    public:

    Stack()
    {
        top = -1;
        count = 0;
    }

    void Push(int value)
    {
        values.push_back(value);
        top = values.back();
        count++;
    }

    int Pop()
    {
        int value = top;
        values.pop_back();
        top = values.back();
        count--;
        
        return value;
    }

    int GetTop()
    {
        return top;
    }

    int GetCount()
    {
        return count;
    }

};