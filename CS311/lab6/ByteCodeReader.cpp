#include <iostream>
#include "./Stack.cpp"

using namespace std;

class ByteCodeReader
{
    private: 

    Stack* stack;

    public:

    ByteCodeReader(Stack* stack)
    {
        this->stack = stack;
    }

    void PushX(int value)
    {
        stack->Push(value);
    }

    int PopX()
    {
        return stack->Pop();
    }

    void Add()
    {
        int a, b;
        a = stack->Pop();
        b = stack->Pop();
        stack->Push(a + b);
    }

    void Sub()
    {
        int a, b;
        a = stack->Pop();
        b = stack->Pop();
        stack->Push(a - b);
    }

    void Mult()
    {
        int a, b;
        a = stack->Pop();
        b = stack->Pop();
        stack->Push(a * b);
    }

    void Div()
    {
        int a, b;
        a = stack->Pop();
        b = stack->Pop();
        stack->Push(a / b);
    }

    void Decl()
    {
        int top = stack->Pop();
        top--;
        stack->Push(top);
    }

    void Incl()
    {
        int top = stack->Pop();
        top++;
        stack->Push(top);
    }

    void Dup()
    {
        int top = stack->GetTop();
        stack->Push(top);
    }

    void Swap()
    {
        int a, b;
        a = stack->Pop();
        b = stack->Pop();
        stack->Push(a);
        stack->Push(b);
    }

    int Print()
    {
        return stack->GetTop();
    }

    void Exit()
    {
        while(stack->GetCount() > 0)
        {
            stack->Pop();
        }
    }

    void ShrX(int shiftVal)
    {
        int top = stack->Pop();
        top >> shiftVal;
        stack->Push(top);
    }

    void ShlX(int shiftVal)
    {
        int top = stack->Pop();
        top << shiftVal;
        stack->Push(top);
    }

    void And()
    {
        int a, b;
        a = stack->Pop();
        b = stack->Pop();
        stack->Push(a & b);
    }

    void Or()
    {
        int a, b;
        a = stack->Pop();
        b = stack->Pop();
        stack->Push(a | b);
    }

    void Xor()
    {
        int a, b;
        a = stack->Pop();
        b = stack->Pop();
        stack->Push(a ^ b);
    }

};