#include <string>

using namespace std;

class Produce
{
    private:

        string name;
        int code;
        int quantity;
        float price;
        Produce *next;

    public:

        Produce(string name, int code, int quantity, float price)
        {
            this->name = name;
            this->code = code;
            this->quantity = quantity;
            this->price = price;
        }

        string GetName()
        {
            return name;
        }

        void SetName(string newName)
        {
            name = newName;
        }

        int GetCode()
        {
            return code;
        }

        void SetCode(int newCode)
        {
            code = newCode;
        }

        int GetQuantity()
        {
            return quantity;
        }

        void SetQuantity(int newQuantity)
        {
            quantity = newQuantity;
        }

        float GetPrice()
        {
            return price;
        }

        void SetPrice(float newPrice)
        {
            price = newPrice;
        }

        void SetNext(Produce *next)
        {
            this->next = next;
        }

        Produce* GetNext()
        {
            return next;
        }
};