#include <iostream>
#include "Produce.cpp"

using namespace std;

class LinkedList
{
    private:

        Produce *head, *tail;

    public:

        LinkedList()
        {
            head = NULL;
            tail = NULL;
        }

        bool NodeExists(int code)
        {
            if(head != NULL)
            {
                Produce *current = head;
                while(current != NULL)
                {
                    if(current->GetCode() == code)
                    {
                        return true;
                    }
                    current = current->GetNext();
                }
            }

            return false;
        }

        int GetIndex(int code)
        {
            int index = 0;
            if(head != NULL)
            {
                Produce *current = head;
                while(current != NULL)
                {
                    if(current->GetCode() == code)
                    {
                        return index;
                    }
                    current = current->GetNext();
                    index++;
                }
            }

            return -1;
        }

        Produce* GetElementByCode(int code)
        {
            if(head != NULL)
            {
                Produce *current = head;
                while(current != NULL)
                {
                    if(current->GetCode() == code)
                    {
                        return current;
                    }
                    current = current->GetNext();
                }
            }

            return NULL;
        }

        void CreateNode(string name, int code, int quantity, float price)
        {
            Produce *temp = new Produce(name, code, quantity, price);
            temp->SetNext(NULL);
            
            if(head == NULL)
            {
                head = temp;
                tail = temp;
                temp = NULL;
            }
            else
            {
                tail->SetNext(temp);
                tail = temp;
            }
        }

        void RemoveNode(int code)
        {
            if(head == NULL)
            {
                // Empty list
                cout << "\nList is empty!";
            }
            else if(head->GetCode() == code)
            {
                // Head is node to be removed
                Produce *temp = head->GetNext();
                free(head);
                head = temp;
            }
            else
            {
                // List is not empty and head is not the node to be removed
                Produce *prev = head;
                while(prev->GetNext() != NULL && prev->GetNext()->GetCode() != code)
                {
                    prev = prev->GetNext();
                }
                prev->SetNext(prev->GetNext()->GetNext());
                free(prev->GetNext());
            }
        }

        void PrintReport()
        {
            if(head != NULL)
            {
                Produce *current = head;
                while(current != NULL)
                {
                    cout << "\n\nName: " << current->GetName();
                    cout << "\nCode: " << current->GetCode();
                    cout << "\nQuantity: " << current->GetQuantity();
                    cout << "\nPrice: " << current->GetPrice();

                    current = current->GetNext();
                }
            }
        }
};