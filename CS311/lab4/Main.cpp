#include <iostream>
#include <fstream>
#include <string>
#include "LinkedList.cpp"

using namespace std;

int main()
{
    int input = 0;
    LinkedList list = LinkedList();
    while(input != 5)
    {
        cout << "\n1. Add item";
        cout << "\n2. Look up item";
        cout << "\n3. Remove item";
        cout << "\n4. Display all items";
        cout << "\n5. Exit\n";
        cin >> input;

        switch(input)
        {
            case 1:
            {
                string name;
                int code, quantity;
                float price;
                cout << "\nEnter name: ";
                cin >> name;
                cout << "\nEnter code: ";
                cin >> code;
                cout << "\nEnter quantity: ";
                cin >> quantity;
                cout << "\nEnter price: ";
                cin >> price;
                list.CreateNode(name, code, quantity, price);
                break;
            }

            case 2:
            {
                int code;
                cout << "\nEnter item code: ";
                cin >> code;
                Produce* item = list.GetElementByCode(code);
                if(item != NULL)
                {
                    cout << "\nName: " << item->GetName();
                    cout << "\nCode: " << item->GetCode();
                    cout << "\nQuantity: " << item->GetQuantity();
                    cout << "\nPrice: " << item->GetPrice();
                }
                else
                {
                    cout << "\nRecord does not exist!\n";
                }
                break;
            }

            case 3:
            {
                int code;
                cout << "\nEnter item code to delete: ";
                cin >> code;
                if(list.NodeExists(code))
                {
                    list.RemoveNode(code);
                }
                else
                {
                    cout << "\nRecord does not exist!\n";
                }
                
            }

            case 4:
            {
                list.PrintReport();
            }

            default:
                break;
        }
    }
}