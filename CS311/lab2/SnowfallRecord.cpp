#include <string>

using namespace std;

class SnowfallRecord
{
    private:

        string location;
        int zip;
        float records [3];

    public:

        SnowfallRecord() { }

        SnowfallRecord(string location, int zip, float records[])
        {
            this->location = location;
            this->zip = zip;
            for(int i = 0; i < 3; i++)
            {
                this->records[i] = records[i];
            }
        }

        int GetZip()
        {
            return zip;
        }

        string GetLocation()
        {
            return location;
        }

        float GetRecords(int year)
        {
            return records[year];
        }

        float GetAverage()
        {
            float total;
            for(float item : records)
            {
                total += item;
            }

            return total / 3;
        }

        float GetMax()
        {
            float max = 0.0;
            for(float item : records)
            {
                if(item > max)
                {
                    max = item;
                }
            }
            
            return max;
        }
};