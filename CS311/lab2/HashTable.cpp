#include "./SnowfallRecord.cpp"

using namespace std;

class HashTable
{
    private:

        static const int SIZE = 17389;
        SnowfallRecord values[SIZE];

    public:

        HashTable() { }

        void AddRecord(SnowfallRecord record)
        {
            int index = record.GetZip() % SIZE;
            values[index] = record;
        }

        SnowfallRecord GetRecord(int zip)
        {
            return values[zip % SIZE];
        }

};