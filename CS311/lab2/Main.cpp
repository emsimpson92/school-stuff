#include <string>
#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include "./HashTable.cpp"

using namespace std;

int main()
{
    ifstream file("./Annual_Snow_Falls_Years-3.dat");
    string line;
    HashTable table = HashTable();

    // Strip off the header
    getline(file, line);
    getline(file, line);

    while(getline(file, line))
    {
        vector<string> elements;
        istringstream iss(line);
        for(line; iss >> line;)
        {
            elements.push_back(line);
        }
        string location = elements[0];
        int zip = stoi(elements[1]);
        float snowfallRecords [3] = { stof(elements[2]), stof(elements[3]), stof(elements[4]) };

        SnowfallRecord record = SnowfallRecord(location, zip, snowfallRecords);

        table.AddRecord(record);
    }

    file.close();

    int zip;
    cout << "Enter a zip code: ";
    cin >> zip;

    SnowfallRecord lookup = table.GetRecord(zip);
    
    if(lookup.GetLocation() == "")
    {
        cout << "Invalid zip code\n";
        
        return -1;
    }

    cout << "Location: " << lookup.GetLocation();
    cout << "\nAverage snowfall: " << lookup.GetAverage();
    cout << "\nMax snowfall: " << lookup.GetMax();

    return 0;
}