#include <string>

using namespace std;

class Task
{
    private:

        string TaskName;
        float ExecutionTime;

    public:

        Task(string taskName, float executionTime)
        {
            this->TaskName = taskName;
            this->ExecutionTime = executionTime;
        }

        string GetName()
        {
            return TaskName;
        }

        void SetName(string newName)
        {
            this->TaskName = newName;
        }

        float GetExecutionTime()
        {
            return ExecutionTime;
        }

        void SetExecutionTime(float time)
        {
            this->ExecutionTime = time;
        }
};