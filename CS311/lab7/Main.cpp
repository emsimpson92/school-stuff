#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include "./Queue.cpp"

using namespace std;

int main()
{
    ifstream file("./Linux_multipleQs-Jobs.dat");
    string line;
    Queue HighPriority = Queue(1);
    Queue MediumPriority = Queue(2);
    Queue LowPriority = Queue(3);
    int HighPriorityTasks = 0;
    int MediumPriorityTasks = 0;
    int LowPriorityTasks = 0;
    float HighPriorityTotalTime = 0;
    float MediumPriorityTotalTime = 0;
    float LowPriorityTotalTime = 0;

    while(getline(file, line))
    {
        vector<string> elements;
        istringstream iss(line);
        for(line; iss >> line;)
        {
            elements.push_back(line);
        }
        string name = elements[0];
        float time = stof(elements[1]);

        Task task = Task(name, time);

        HighPriority.Enqueue(task);
    }

    file.close();

    for(int i = 0; i < HighPriority.GetCount(); i++)
    {
        Task t = HighPriority.GetItemAtIndex(i);
        if(t.GetExecutionTime() > 103)
        {
            LowPriorityTasks++;
            LowPriorityTotalTime += t.GetExecutionTime();
        }
        else if(t.GetExecutionTime() > 3)
        {
            MediumPriorityTasks++;
            MediumPriorityTotalTime += t.GetExecutionTime();
        }
        else
        {
            HighPriorityTasks++;
            HighPriorityTotalTime += t.GetExecutionTime();
        }
    }

    while(!HighPriority.IsEmpty())
    {
        Task t = HighPriority.GetCurrent();
        t.SetExecutionTime(t.GetExecutionTime() - 3);
        if(t.GetExecutionTime() > 0)
        {
            MediumPriority.Enqueue(t);
        }
        
        HighPriority.Dequeue();
    }

    while(!MediumPriority.IsEmpty())
    {
        Task t = MediumPriority.GetCurrent();
        t.SetExecutionTime(t.GetExecutionTime() - 100);
        if(t.GetExecutionTime() > 0)
        {
            LowPriority.Enqueue(t);
        }

        MediumPriority.Dequeue();
    }

    while(!LowPriority.IsEmpty())
    {
        LowPriority.Dequeue();
    }

    cout << "\nTotal high priority tasks: " << HighPriorityTasks;
    cout << "\nTotal medium priority tasks: " << MediumPriorityTasks;
    cout << "\nTotal low priority tasks: " << LowPriorityTasks;
    cout << "\n\nAverage high priority execution time: " << HighPriorityTotalTime / HighPriorityTasks;
    cout << "\nAverage medium priority execution time: " << MediumPriorityTotalTime / MediumPriorityTasks;
    cout << "\nAverage low priority execution time: " << LowPriorityTotalTime / LowPriorityTasks;

    return 0;
}