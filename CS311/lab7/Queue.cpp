#include <string>
#include <vector>
#include "./Task.cpp"

using namespace std;

class Queue
{
    private:

        vector<Task> data;
        int PriorityLevel;

    public:
        
        Queue(int priorityLevel)
        {
            this->PriorityLevel = priorityLevel;
        }

        Task GetCurrent()
        {
            return data.front();
        }

        Task GetItemAtIndex(int index)
        {
            return data.at(index);
        }
        
        int GetCount()
        {
            return data.size();
        }

        void Dequeue()
        {
            if(!data.empty())
            {
                data.erase(data.begin());
            }
        }

        void Enqueue(Task t)
        {
            data.push_back(t);
        }

        bool IsEmpty()
        {
            return data.empty();
        }
};