#include <ctime>

using namespace std;

class Task
{
    private:

        int taskNumber;
        tm arrivalTime;

    public:

        Task(int taskNumber, tm arrivalTime)
        {
            this->taskNumber = taskNumber;
            this->arrivalTime = arrivalTime;
        }

        int GetTaskNumber()
        {
            return taskNumber;
        }

        void SetTaskNumber(int taskNumber)
        {
            this->taskNumber = taskNumber;
        }

        tm GetArrivalTime()
        {
            return arrivalTime;
        }

        void SetArrivalTime(tm arrivalTime)
        {
            this->arrivalTime = arrivalTime;
        }

};