#include <ctime>

using namespace std;

class Server
{
    private:

        int currentTask;
        tm arrivalTime;
        tm startTime;
        tm waitTime;
        tm serviceTime;

    public:

        Server(int currentTask)
        {
            this->currentTask = currentTask;
        }

        int GetCurrentTask()
        {
            return currentTask;
        }

        void SetCurrentTask(int currentTask)
        {
            this->currentTask = currentTask;
        }

        tm GetArrivalTime()
        {
            return arrivalTime;
        }

        void SetArrivalTime(tm arrivalTime)
        {
            this->arrivalTime = arrivalTime;
        }

        tm GetStartTime()
        {
            return startTime;
        }

        void SetStartTime(tm startTime)
        {
            this->startTime = startTime;
        }

        tm GetWaitTime()
        {
            return waitTime;
        }

        void SetWaitTime(tm waitTime)
        {
            this->waitTime = waitTime;
        }

        tm GetServiceTime()
        {
            return serviceTime;
        }

        void SetServiceTime(tm serviceTime)
        {
            this->serviceTime = serviceTime;
        }
};