#include <ctime>
#include <iostream>

using namespace std;

class SimulationStatistics
{
    private:

        int totalTaskCount;
        tm totalServiceTime;
        tm totalWaitTime;
        int peakQueueSize;

    public:

        SimulationStatistics()
        {
            this->peakQueueSize = 0;
        }

        int GetTotalCount()
        {
            return totalTaskCount;
        }

        void UpdateStats(int queueSize, tm serviceTime, tm waitTime)
        {
            AddTask();
            AddServiceTime(serviceTime);
            AddWaitTime(waitTime);
            UpdatePeakQueueSize(queueSize);
        }

        void AddTask()
        {
            totalTaskCount++;
        }

        void AddServiceTime(tm time)
        {
            totalServiceTime.tm_sec += time.tm_sec;
            totalServiceTime.tm_min += time.tm_min;
            totalServiceTime.tm_hour += time.tm_hour;
        }

        void AddWaitTime(tm time)
        {
            totalWaitTime.tm_sec += time.tm_sec;
            totalWaitTime.tm_min += time.tm_min;
            totalWaitTime.tm_hour += time.tm_hour;
        }

        void UpdatePeakQueueSize(int size)
        {
            if(size > peakQueueSize)
            {
                peakQueueSize = size;
            }
        }

        tm GetAvgTaskServiceTime()
        {
            tm avg;
            avg.tm_sec = totalServiceTime.tm_sec / totalTaskCount;
            avg.tm_min = totalServiceTime.tm_min / totalTaskCount;
            avg.tm_hour = totalServiceTime.tm_hour / totalTaskCount;

            return avg;
        }

        tm GetAvgTaskWaitTime()
        {
            tm avg;
            avg.tm_sec = totalWaitTime.tm_sec / totalTaskCount;
            avg.tm_min = totalWaitTime.tm_min / totalTaskCount;
            avg.tm_hour = totalWaitTime.tm_hour / totalTaskCount;

            return avg;        
        }

        void PrintReport()
        {
            tm averageServiceTime = GetAvgTaskServiceTime();
            tm averageWaitTime = GetAvgTaskWaitTime();

            cout << "\nTotal task count: " << totalTaskCount;
            cout << "\nTotal service time: " << totalServiceTime.tm_hour << "h, " << totalServiceTime.tm_min << "m, " << totalServiceTime.tm_sec << "s";
            cout << "\nTotal wait time: " << totalWaitTime.tm_hour << "h, " << totalWaitTime.tm_min << "m, " << totalWaitTime.tm_sec << "s";
            cout << "\nPeak queue size: " << peakQueueSize;
            cout << "\nAverage service time: " << averageServiceTime.tm_hour << "h, " << averageServiceTime.tm_min << "m, " << averageServiceTime.tm_sec << "s";
            cout << "\nAverage wait time: " << averageWaitTime.tm_hour << "h " << averageWaitTime.tm_min << "m " << averageWaitTime.tm_sec << "s";            
        }
};