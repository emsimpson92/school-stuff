#include <iostream>
#include <string>
#include "./LinkedList.cpp"

using namespace std;

int main()
{
    LinkedList list = LinkedList();
    list.CreateNode("Ada", 1, 1864, 392365, 1055);
    list.CreateNode("Bonner", 17, 1907, 40877, 1738);
    list.CreateNode("Fremont", 43, 1893, 13242, 1867);
    list.CreateNode("Madison", 65, 1913, 37536, 472);
    list.CreateNode("Butte", 23, 1917, 2891, 2233);
    list.CreateNode("Bannock", 5, 1893, 82839, 1113);

    int input;
    cout << "1. Query a specific county";
    cout << "\n2. Print report\n";
    cin >> input;
    if(input == 1)
    {
        string countyName;
        cout << "\nEnter county name: ";
        cin >> countyName;
        County* county = list.GetElementByName(countyName);
        if(county != NULL)
        {
            cout << "\nName: " << county->GetName();
            cout << "\nFIPSCode: " << county->GetFIPSCode();
            cout << "\nEstablished: " << county->GetEstablished();
            cout << "\nPopulation: " << county->GetPopulation();
            cout << "\nArea (sq ft.): " << county->GetArea() << "\n";
        }
        else
        {
            cout << "\nNo county found!\n";
        }
        
    }
    else if(input == 2)
    {
        cout << "\nLargest population: " << list.GetLargestPop();
        cout << "\nLargest area: " << list.GetLargestArea();
        cout << "\nOldest county: " << list.GetOldest() << "\n";
    }

    return 0;
}