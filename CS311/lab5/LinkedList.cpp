#include <iostream>
#include "./County.cpp"

using namespace std;

class LinkedList
{
    private:

        County *head, *tail;

    public:

        LinkedList()
        {
            head = NULL;
            tail = NULL;
        }

        bool NodeExists(string name)
        {
            if(head != NULL)
            {
                County *current = head;
                while(current != NULL)
                {
                    if(current->GetName() == name)
                    {
                        return true;
                    }
                    current = current->GetNext();
                }
            }

            return false;
        }

        int GetIndex(string name)
        {
            int index = 0;
            if(head != NULL)
            {
                County *current = head;
                while(current != NULL)
                {
                    if(current->GetName() == name)
                    {
                        return index;
                    }
                    current = current->GetNext();
                    index++;
                }
            }

            return -1;
        }

        County* GetElementByName(string name)
        {
            if(head != NULL)
            {
                County *current = head;
                while(current != NULL)
                {
                    if(current->GetName() == name)
                    {
                        return current;
                    }
                    current = current->GetNext();
                }
            }

            return NULL;
        }

        void CreateNode(string name, int FIPSCode, int established, int population, int area)
        {
            County *temp = new County(name, FIPSCode, established, population, area);
            
            if(head == NULL || head->GetName() >= temp->GetName())
            {
                temp->SetNext(head);
                head = temp;
            }
            else
            {
                County *current = head;
                while(current->GetNext() != NULL && current->GetNext()->GetName() < temp->GetName())
                {
                    current = current->GetNext();
                }
                temp->SetNext(current->GetNext());
                current->SetNext(temp);
            }
        }

        void RemoveNode(string name)
        {
            if(head == NULL)
            {
                // Empty list
                cout << "\nList is empty!";
            }
            else if(head->GetName() == name)
            {
                // Head is node to be removed
                County *temp = head->GetNext();
                free(head);
                head = temp;
            }
            else
            {
                // List is not empty and head is not the node to be removed
                County *prev = head;
                while(prev->GetNext() != NULL && prev->GetNext()->GetName() != name)
                {
                    prev = prev->GetNext();
                }
                prev->SetNext(prev->GetNext()->GetNext());
                free(prev->GetNext());
            }
        }

        string GetLargestPop()
        {
            int temp = 0;
            string county;
            if(head != NULL)
            {
                County *current = head;
                while(current != NULL)
                {
                    if(current->GetPopulation() > temp)
                    {
                        temp = current->GetPopulation();
                        county = current->GetName();
                    }
                    current = current->GetNext();
                }
            }

            return county;
        }

        string GetLargestArea()
        {
            int temp = 0;
            string county;
            if(head != NULL)
            {
                County *current = head;
                while(current != NULL)
                {
                    if(current->GetArea() > temp)
                    {
                        temp = current->GetArea();
                        county = current->GetName();
                    }
                    current = current->GetNext();
                }
            }

            return county;
        }

        string GetOldest()
        {
            int temp = 9999;
            string county;
            if(head != NULL)
            {
                County *current = head;
                while(current != NULL)
                {
                    if(current->GetEstablished() < temp)
                    {
                        temp = current->GetEstablished();
                        county = current->GetName();
                    }
                    current = current->GetNext();
                }
            }

            return county;
        }
};