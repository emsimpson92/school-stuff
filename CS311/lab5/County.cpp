#include <string>

using namespace std;

class County
{
    private:

        string name;
        int FIPSCode;
        int established;
        int population;
        int area;
        County *next;

    public:

        County(string name, int FIPSCode, int established, int population, int area)
        {
            this->name = name;
            this->FIPSCode = FIPSCode;
            this->established = established;
            this->population = population;
            this->area = area;
        }

        string GetName()
        {
            return name;
        }

        void SetNext(County *next)
        {
            this->next = next;
        }

        int GetFIPSCode()
        {
            return FIPSCode;
        }

        int GetEstablished()
        {
            return established;
        }

        int GetPopulation()
        {
            return population;
        }

        int GetArea()
        {
            return area;
        }

        County* GetNext()
        {
            return next;
        }
};