#include <iostream>
#include <sstream>
#include <fstream>
#include <vector>
#include <string>
#include "./simpleBSTclass.h"

using namespace std;

int main()
{
    ifstream file("./passenger_info.txt");
    string line;
	BSTree *tree;
	tree = createBSTree();

    // Strip off the header
    getline(file, line);
    getline(file, line);

    while(getline(file, line))
    {
        vector<string> elements;
        istringstream iss(line);
        for(line; iss >> line;)
        {
            elements.push_back(line);
        }
        string name = elements[0];
        int miles = stoi(elements[1]);
        int years = stoi(elements[2]);
        int number = stoi(elements[3]);

        tree->insertBST(name, miles, years, number);
    }

    file.close();

    tree->inOrderBST();
    cout << "Results exported to results.txt\n";
    cout << "\nHighest priority:\n";
    tree->findSmallestBST();
    cout << "\nLowest priority:\n";
    tree->findLargestBST();

    return 0;
}