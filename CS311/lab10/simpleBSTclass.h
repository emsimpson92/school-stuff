#include <iostream>
#include <string>
#include <fstream>

using namespace std;

class BSTree{
	private:
		struct BSTnode{
			string name;
			int priority;
			int miles;
			int years;
			int number;

			BSTnode *left_child;
			BSTnode *right_child;

			BSTnode(string myname, int mymiles, int myyears, int mynumber, BSTnode *left=nullptr, BSTnode *right=nullptr)
			{
				name = myname;
				miles = mymiles;
				years = myyears;
				number = mynumber;
				priority = (mymiles/1500) + myyears - mynumber;
				left_child  = left;
				right_child = right;
			}
		};

		BSTnode *root;

		void insertBST(BSTnode *&node,string name,int miles,int years,int number);
		void inOrderBST(BSTnode *node);
		void  findSmallestBST(BSTnode *node);
		void  findLargestBST(BSTnode *node);
	public:

		BSTree();
		void insertBST(string name,int miles,int years,int number);
		void inOrderBST();
		void  findSmallestBST();
		void  findLargestBST();
};

BSTree::BSTree()
{
	root  = nullptr;
}

void BSTree::findSmallestBST(BSTnode *node)
{
	while(node->left_child != nullptr)
	{
		node = node->left_child;
	}
	cout << "Name: " << node->name << "\n";
	cout << "Miles: " << node->miles << "\n";
	cout << "Years: " << node->years << "\n";
	cout << "Helpdesk number: " << node->number << "\n";
}

void BSTree::findSmallestBST()
{
	return findSmallestBST(root);
}

void BSTree::findLargestBST(BSTnode *node)
{
	while(node->right_child != nullptr)
	{
		node = node->right_child;
	}
	cout << "Name: " << node->name << "\n";
	cout << "Miles: " << node->miles << "\n";
	cout << "Years: " << node->years << "\n";
	cout << "Helpdesk number: " << node->number << "\n";
}

void BSTree::findLargestBST()
{
	return findLargestBST(root);
}

void BSTree::insertBST(string name, int miles, int years, int number)
{
	insertBST(root,name,miles,years,number);
}

void BSTree::insertBST(BSTnode *&root,string name,int miles,int years,int number)
{
	float priority = (miles/1500) + years - number;
	if(root==nullptr)
		root = new BSTnode(name, miles, years, number);
	else{
		if(priority < root->priority )
			insertBST(root->left_child, name, miles, years, number);
		else
			insertBST(root->right_child, name, miles, years, number);
	}

	return;
}

void BSTree::inOrderBST(BSTnode *node)
{
	if(node != nullptr){
		inOrderBST(node->left_child);
		ofstream file("./results.txt", ios_base::app);
		file << node->name << "  " << node->priority << "\n";
		file.close();
		inOrderBST(node->right_child);
	}

	return;
}

void BSTree::inOrderBST()
{
	inOrderBST(root);

	return;
}

BSTree *createBSTree()
{
	BSTree *node;

	try{
		node = new BSTree();
	}
	catch(bad_alloc){
		cout << "\n\n\t\t\tYikes! Out of memory!!!\n";
		node = nullptr;
	}

	return node;
}