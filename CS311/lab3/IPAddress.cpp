#include <string>

using namespace std;

class IPAddress
{
    private:

        string IP;
        int count;
        IPAddress *next;

    public:

        IPAddress(string IP)
        {
            this->IP = IP;
            this->count = 1;
        }

        string GetIP()
        {
            return IP;
        }

        void SetIP(string newIP)
        {
            IP = newIP;
        }

        int GetCount()
        {
            return count;
        }

        void UpdateCount()
        {
            count++;
        }

        void SetNext(IPAddress *next)
        {
            this->next = next;
        }

        IPAddress* GetNext()
        {
            return next;
        }
};