#include "IPAddress.cpp"
#include <iostream>

using namespace std;

class LinkedList
{
    private:

        IPAddress *head, *tail;

    public:

        LinkedList()
        {
            head = NULL;
            tail = NULL;
        }

        bool NodeExists(string IP)
        {
            if(head != NULL)
            {
                IPAddress *current = head;
                while(current != NULL)
                {
                    if(current->GetIP() == IP)
                    {
                        return true;
                    }
                    current = current->GetNext();
                }
            }

            return false;
        }

        int GetIndex(string IP)
        {
            int index = 0;
            if(head != NULL)
            {
                IPAddress *current = head;
                while(current != NULL)
                {
                    if(current->GetIP() == IP)
                    {
                        return index;
                    }
                    current = current->GetNext();
                    index++;
                }
            }

            return -1;
        }

        IPAddress* GetElementByIP(string IP)
        {
            if(head != NULL)
            {
                IPAddress *current = head;
                while(current != NULL)
                {
                    if(current->GetIP() == IP)
                    {
                        return current;
                    }
                    current = current->GetNext();
                }
            }

            return NULL;
        }

        void CreateNode(string IP)
        {
            IPAddress *temp = new IPAddress(IP);
            temp->SetNext(NULL);
            
            if(head == NULL)
            {
                head = temp;
                tail = temp;
                temp = NULL;
            }
            else
            {
                if(!NodeExists(IP))
                {
                    tail->SetNext(temp);
                    tail = temp;
                }
                else
                {
                    IPAddress* address = GetElementByIP(IP);
                    address->UpdateCount();
                }   
            }
        }

        void PrintReport()
        {
            if(head != NULL)
            {
                cout << "\nIP Address\t\tNumber of Accesses";
                cout << "\n-------------------------------------------";
                IPAddress *current = head;
                while(current != NULL)
                {
                    cout << "\n" << current->GetIP() << "\t\t\t" << current->GetCount();
                    current = current->GetNext();
                }
            }
        }
};