#include <iostream>
#include <fstream>
#include <string>
#include "LinkedList.cpp"

using namespace std;

int main()
{
    ifstream file("./Residence_Hall_IPs-10000.dat");
    string line;
    LinkedList list = LinkedList();

    while(getline(file, line))
    {
        line = line.erase(line.size() - 1);
        list.CreateNode(line);
    }

    file.close();

    int selection;
    string IP;
    cout << "Please enter a value";
    cout << "\n1. Query specific IP";
    cout << "\n2. Generate IP report\n";
    cin >> selection;
    if(selection == 1)
    {
        cout << "\nEnter IP\n";
        cin >> IP;
        IPAddress* address = list.GetElementByIP(IP);
        if(address != NULL)
        {
            cout << "\nIP: " << address->GetIP();
            cout << "\nIndex in list: " << list.GetIndex(IP);
            cout << "\nNumber of Accesses: " << address->GetCount() << "\n";
        }
        else
        {
            cout << "\nRecord does not exist!\n";
        }
    }
    else if(selection == 2)
    {
        list.PrintReport();
    }
    else
    {
        cout << "\nInvalid selection.\n";
    }

    return 0;
}