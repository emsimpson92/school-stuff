#include <string>

using namespace std;

class Item
{
    private:

    string name;
    float price;
    int quantity;
    Item *next;

    public:

    Item(string name, float price, int quantity)
    {
        this->name = name;
        this->price = price;
        this->quantity = quantity;
    }

    string GetName()
    {
        return name;
    }

    void SetName(string newName)
    {
        this->name = newName;
    }

    float GetPrice()
    {
        return price;
    }

    void SetPrice(float newPrice)
    {
        this->price = newPrice;
    }

    int GetQuantity()
    {
        return quantity;
    }

    void SetQuantity(int newQuantity)
    {
        this->quantity = newQuantity;
    }

    Item* GetNext()
    {
        return next;
    }

    void SetNext(Item *next)
    {
        this->next = next;
    }
};