#include <iostream>
#include <string>
#include <vector>
#include <fstream>
#include <sstream>
#include "./LinkedList.cpp"

using namespace std;

void BuildDb(LinkedList *list)
{
    ifstream file("./store_inventory.txt");
    string line;
    getline(file, line);
    getline(file, line);

    while(getline(file, line))
    {
        vector<string> elements;
        istringstream iss(line);
        for(line; iss >> line;)
        {
            elements.push_back(line);
        }
        string name = elements[0];
        float price = stof(elements[1]);
        int quantity = stoi(elements[2]);

        list->CreateNode(name, price, quantity);
    }

    file.close();
}

void AddItem(LinkedList *list)
{
    bool done = false;
    string name;
    float price;
    int qty;
    while(!done)
    {
        cout << "\nEnter name: ";
        cin >> name;
        cout << "\nEnter price: ";
        cin >> price;
        cout << "\nEnter quantity: ";
        cin >> qty;
        
        list->CreateNode(name, price, qty);

        cout << "\nEnter another? (y/n)\n";
        string answer;
        cin >> answer;
        if(answer == "n")
        {
            done = true;
        }
    }
}

void RemoveItem(LinkedList *list)
{
    string name;
    bool done = false;
    while(!done)
    {
        cout << "\nEnter name: ";
        cin >> name;
        if(list->NodeExists(name))
        {
            list->RemoveNode(name);
        }
        else
        {
            cout << "\nNode does not exist!\n";
        }
        cout << "\nRemove another? (y/n)\n";
        string answer;
        cin >> answer;
        if(answer == "n")
        {
            done = true;
        }
    }
}

void EditItem(LinkedList *list)
{
    string name;
    cout << "\nEnter item name: ";
    cin >> name;
    if(list->NodeExists(name))
    {
        float price;
        int quantity;
        cout << "\nEnter new price: ";
        cin >> price;
        cout << "\nEnter new quantity: ";
        cin >> quantity;
        Item *item = list->GetElementByName(name);
        item->SetPrice(price);
        item->SetQuantity(quantity);
    }
    else
    {
        cout << "\nNode does not exist!\n";
    }
    
}

void GetItem(LinkedList *list)
{
    string name;
    cout << "\nEnter item name: ";
    cin >> name;
    if(list->NodeExists(name))
    {
        Item *item = list->GetElementByName(name);
        cout << "\nItem name: " << item->GetName();
        cout << "\nItem price: " << item->GetPrice();
        cout << "\nItem quantity: " << item->GetQuantity();
    }
    else
    {
        cout << "\nNode does not exist!\n";
    }
}

void PrintAll(LinkedList *list)
{
    list->PrintReport();
}

int main()
{
    LinkedList list = LinkedList();
    bool done = false;

    while(!done)
    {
        cout << "\nInventory Database Menu:\n\n";
        cout << "1. Build Database\n";
        cout << "2. Add New Item\n";
        cout << "3. Remove Item\n";
        cout << "4. Edit Item\n";
        cout << "5. Search For Item\n";
        cout << "6. List All Items\n";
        cout << "7. Exit\n\n";
        cout << "Enter Choice: ";
        int input;
        cin >> input;

        switch(input)
        {
            case 1:
                BuildDb(&list);
                break;

            case 2:
                AddItem(&list);
                break;

            case 3:
                RemoveItem(&list);
                break;

            case 4:
                EditItem(&list);
                break;

            case 5:
                GetItem(&list);
                break;

            case 6:
                PrintAll(&list);
                break;

            default:
                done = true;
                break;
        }
    }

    return 0;
}