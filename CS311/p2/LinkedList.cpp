#include <iostream>
#include "./Item.cpp"

using namespace std;

class LinkedList
{
    private:

        Item *head, *tail;

    public:

        LinkedList()
        {
            head = NULL;
            tail = NULL;
        }

        bool NodeExists(string name)
        {
            if(head != NULL)
            {
                Item *current = head;
                while(current != NULL)
                {
                    if(current->GetName() == name)
                    {
                        return true;
                    }
                    current = current->GetNext();
                }
            }

            return false;
        }

        int GetIndex(string name)
        {
            int index = 0;
            if(head != NULL)
            {
                Item *current = head;
                while(current != NULL)
                {
                    if(current->GetName() == name)
                    {
                        return index;
                    }
                    current = current->GetNext();
                    index++;
                }
            }

            return -1;
        }

        Item* GetElementByName(string name)
        {
            if(head != NULL)
            {
                Item *current = head;
                while(current != NULL)
                {
                    if(current->GetName() == name)
                    {
                        return current;
                    }
                    current = current->GetNext();
                }
            }

            return NULL;
        }

        void CreateNode(string name, float price, int quantity)
        {
            Item *temp = new Item(name, price, quantity);
            
            if(head == NULL || head->GetName() >= temp->GetName())
            {
                temp->SetNext(head);
                head = temp;
            }
            else
            {
                Item *current = head;
                while(current->GetNext() != NULL && current->GetNext()->GetName() < temp->GetName())
                {
                    current = current->GetNext();
                }
                temp->SetNext(current->GetNext());
                current->SetNext(temp);
            }
        }

        void RemoveNode(string name)
        {
            if(head == NULL)
            {
                // Empty list
                cout << "\nList is empty!";
            }
            else if(head->GetName() == name)
            {
                // Head is node to be removed
                Item *temp = head->GetNext();
                free(head);
                head = temp;
            }
            else
            {
                // List is not empty and head is not the node to be removed
                Item *prev = head;
                while(prev->GetNext() != NULL && prev->GetNext()->GetName() != name)
                {
                    prev = prev->GetNext();
                }
                prev->SetNext(prev->GetNext()->GetNext());
                free(prev->GetNext());
            }
        }

        void PrintReport()
        {
            if(head != NULL)
            {
                Item *current = head;
                while(current != NULL)
                {
                    cout << "\n\nName: " << current->GetName();
                    cout << "\nPrice: " << current->GetPrice();
                    cout << "\nQuantity: " << current->GetQuantity();

                    current = current->GetNext();
                }
            }
        }
};