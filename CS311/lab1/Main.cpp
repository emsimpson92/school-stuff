#include <string>
#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include "./Customer.cpp"

using namespace std;

int main()
{
    ifstream file("./Customer_data-sorted.txt");
    string line;
    vector<Customer> customers;

    // Strip off the header
    getline(file, line);
    getline(file, line);

    while(getline(file, line))
    {
        vector<string> elements;
        istringstream iss(line);
        for(line; iss >> line;)
        {
            elements.push_back(line);
        }
        string name = elements[0];
        int Id = stoi(elements[1]);
        int age = stoi(elements[2]);
        float balance = stof(elements[3]);

        Customer customer = Customer(name, Id, age, balance);

        customers.push_back(customer);
    }

    file.close();

    int input, first, last, middle, comparisons;
    cout << "Enter customer ID number: ";
    cin >> input;
    first = 0;
    last = customers.size() - 1;
    middle = (first + last) / 2;
    comparisons = 0;

    while(first <= last)
    {
        comparisons ++;
        if(customers[middle].GetId() < input)
        {
            first = middle + 1;
        }
        else if(customers[middle].GetId() == input)
        {
            // Found them
            cout << "Found customer in " << comparisons << " iterations.\n";
            cout << "Customer location in records: " << middle + 1 << "\n";
            cout << "Name: " << customers[middle + 1].GetName() << "\n";
            cout << "Age: " << customers[middle + 1].GetAge() << "\n";
            cout << "Balance: " << customers[middle + 1].GetBalance() << "\n";

            break;
        }
        else
        {
            last = middle - 1;
        }
        middle = (first + last) / 2;
    }
    if(first > last)
    {
        // Not found
        cout << "\nCustomer not found!";
    }

    return 0;
}