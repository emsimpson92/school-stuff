#include <string>

using namespace std;

class Customer
{
    private:

        string name;
        int Id;
        int age;
        float balance;

    public:

        Customer(string name, int Id, int age, float balance)
        {
            this->name = name;
            this->Id = Id;
            this->age = age;
            this->balance = balance;
        }

        int GetId()
        {
            return Id;
        }

        string GetName()
        {
            return name;
        }

        int GetAge()
        {
            return age;
        }

        float GetBalance()
        {
            return balance;
        }
};