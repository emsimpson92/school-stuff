#include <fstream>
#include <sstream>
#include <string>
#include <vector>
#include <cstdlib>
#include <ctime>
#include <iostream>
#include <iomanip>
#include "./Student.cpp"

using namespace std;

void ShellSort(Student students[])
{
    int i,j,k;
	const int NUM = 10000;
	int current;
	int last;
	int value;
	int index;
	int increment;

	last = NUM -1;
	increment = last/2;

	while(increment > 0){
	
		current = increment;
	
		while(current <= last){

			value = students[current].GetId();
			index = current - increment;
		
			while(index >=0 && value < students[index].GetId()){
				//Keep shifting value back towards higher index:
				students[index+increment] = students[index];
				index -= increment;
			
			}//while(index)

			students[index+increment].SetId(value);
			current += 1;

		}// while(current)

		increment /= 2;

	}// while(increment)
}

int main()
{
    Student students[10000];
    ifstream file("./LCSC-Student_Records.dat");
    string line;
    int current = 0;

    getline(file, line);
    while(getline(file, line))
    {
        vector<string> elements;
        istringstream iss(line);
        for(line; iss >> line;)
        {
            elements.push_back(line);
        }
        int studentId = stoi(elements[0]);
        float GPA = stof(elements[1]);
        int age = stoi(elements[2]);
        int totalCredits = stoi(elements[3]);

        students[current] = Student(studentId, GPA, age, totalCredits);
        current++;
    }

    ShellSort(students);

    file.close();

	ofstream outFile("output.dat");
	outFile << "Student ID	Cumulative GPA	Age	Total Course Credits\n";
	for(Student student : students)
	{
		outFile << student.GetId() << "\t" << student.GetGPA() << "\t" << student.GetAge() << "\t" << student.GetCredits() << "\n";
	}

	outFile.close();

    return 0;
}