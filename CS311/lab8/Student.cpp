using namespace std;

class Student
{
    private:

        int studentId;
        float GPA;
        int age;
        int totalCredits;

    public:

        Student() { }

        Student(int studentId, float GPA, int age, int totalCredits)
        {
            this->studentId = studentId;
            this->GPA = GPA;
            this->age = age;
            this->totalCredits = totalCredits;
        }

        int GetId()
        {
            return studentId;
        }

        void SetId(int newId)
        {
            this->studentId = newId;
        }

        float GetGPA()
        {
            return GPA;
        }

        void SetGPA(float newGpa)
        {
            this->GPA = newGpa;
        }

        int GetAge()
        {
            return age;
        }

        void SetAge(int newAge)
        {
            this->age = newAge;
        }

        int GetCredits()
        {
            return totalCredits;
        }

        void SetCredits(int newCredits)
        {
            this->totalCredits = newCredits;
        }

};