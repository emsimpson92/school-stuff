#include <iostream>
#include <iostream>
#include <iomanip>
#include <string>
#include <stdlib.h>

using namespace std;

#include "simpleBSTclass.h"

int main()
{
	int i,j,k;

	BSTree *tree;

	tree = createBSTree();

	// insert 50 random values
	for(int i = 0; i < 50; i++)
	{
		tree->insertBST(rand() % 1000 + 1);
	}
	// in order traversal
	tree->inOrderBST();

	cout << "\n\t\t\tMinimum  : " << tree->findSmallestBST();
	cout << "\n\t\t\tMaximum  : " << tree->findLargestBST();

	tree->deleteBST(tree->findLargestBST());
	tree->deleteBST(tree->findSmallestBST());

	cout << "\n\t\t\tSum  : " << tree->sumBST() << "\n";
	
	return 0;
}

