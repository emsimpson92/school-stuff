#include <iostream>

using namespace std;

class BSTree{
	private:
		struct BSTnode{
			int key;
			BSTnode *left_child;
			BSTnode *right_child;

			BSTnode(int val, BSTnode *left=nullptr, BSTnode *right=nullptr)
			{
				key = val;
				left_child  = left;
				right_child = right;
			}
		};

		BSTnode *root;

		void insertBST(BSTnode *&node,int key);
		void inOrderBST(BSTnode *node);
		void preOrderBST(BSTnode *node);
		void postOrderBST(BSTnode *node);
		void findLeafBST(BSTnode *node);
		int  findSmallestBST(BSTnode *node);
		int  findLargestBST(BSTnode *node);
		int  getTreeSizeBST(BSTnode *node);
		bool deleteBST(BSTnode *&node,int key);
		BSTnode *searchBST(BSTnode *node, int key);
		void sumBST(BSTnode *node);
	public:

		BSTree();
		bool deleteBST(int key);
		void insertBST(int key);
		BSTnode *searchBST(int key);
		void inOrderBST();
		void preOrderBST();
		void postOrderBST();
		void findLeafBST();
		int  findSmallestBST();
		int  findLargestBST();
		bool isEmptyList();
		int getTreeSizeBST();
		int sumBST();

		int sum;
};

BSTree::BSTree()
{
	root  = nullptr;
}

bool BSTree::isEmptyList()
{
	return root == nullptr;
}

int BSTree::findSmallestBST(BSTnode *node)
{
	if(node->left_child == nullptr)
		return node->key;

	return findSmallestBST(node->left_child);
}

int BSTree::findSmallestBST()
{
	return findSmallestBST(root);
}

int BSTree::findLargestBST(BSTnode *node)
{
	if(node->right_child == nullptr)
		return node->key;

	return findLargestBST(node->right_child);
}

int BSTree::findLargestBST()
{
	return findLargestBST(root);
}

int BSTree::getTreeSizeBST(BSTnode *node)
{
	if(node==nullptr){
		return 0;
	}
	else{
		return 1 + getTreeSizeBST(node->left_child) + getTreeSizeBST(node->right_child);
	}
}

void BSTree::findLeafBST(BSTnode *node)
{
	if(node->left_child == nullptr && node->right_child == nullptr)
		cout <<"\n\t\t\tLeaf Node Found: " << node->key <<endl;
	
	if(node->left_child != nullptr)
		findLeafBST(node->left_child);

	if(node->right_child != nullptr)
		findLeafBST(node->right_child);

	return;
}

void BSTree::findLeafBST()
{
	findLeafBST(root);

	return;
}

int BSTree::getTreeSizeBST()
{
	return getTreeSizeBST(root);
}

bool BSTree::deleteBST(BSTnode *&node,int key)
{
	BSTnode *rem;

	if(node==nullptr){
		return false;
	}

	if(key < node->key){
		return deleteBST(node->left_child, key);
	}
	else if(key > node->key){
		return deleteBST(node->right_child, key);
	}
	else{
		if(node->left_child == nullptr){
			rem  = node;
			node = node->right_child;
			return true;			
		}
		else if(node->right_child == nullptr){
			rem  = node;
			node = node->left_child;
			return true;			
		}
		else{
			rem = node->left_child;
			
			while(rem->right_child != nullptr)
				rem = rem->right_child;
			
			node->key = rem->key;
			return deleteBST(node->left_child, rem->key);

		}
	}

}

bool BSTree::deleteBST(int key)
{
	bool success;
	
	success = deleteBST(root,key);

	return success;
}


void BSTree::insertBST(int key)
{
	insertBST(root,key);
}

void BSTree::insertBST(BSTnode *&root,int key)
{
	if(root==nullptr)
		root = new BSTnode(key);
	else{
		if(key < root->key )
			insertBST(root->left_child, key);
		else
			insertBST(root->right_child, key);
	}

	return;
}

BSTree::BSTnode *BSTree::searchBST(BSTnode *node, int key)
{

	if(node==nullptr){
		cout << "\n\t\t\tKey NOT Found Node!!!:" << key <<"\n\n";
		return node;
	}

	if(key < node->key){
		return searchBST(node->left_child, key);
	}
	else if(key > node->key){
		return searchBST(node->right_child, key);
	}
	else{
		cout << "\n\t\t\tFound Node With Key:" << key << " vs. " << node->key <<"\n\n";
		return node;
	}
}

BSTree::BSTnode *BSTree::searchBST(int key)
{
	BSTnode *result;

	result = searchBST(root,key);

	return result;
}

void BSTree::inOrderBST(BSTnode *node)
{
	if(node != nullptr){
		inOrderBST(node->left_child);
		cout <<"\t\t\t" << node->key << endl;
		inOrderBST(node->right_child);
	}

	return;
}

void BSTree::inOrderBST()
{
	inOrderBST(root);

	return;
}

void BSTree::preOrderBST(BSTnode *node)
{
	if(node != nullptr){
		cout <<"\t\t\t" << node->key << endl;
		preOrderBST(node->left_child);
		preOrderBST(node->right_child);
	}

	return;
}

void BSTree::preOrderBST()
{
	preOrderBST(root);

	return;
}

void BSTree::postOrderBST(BSTnode *node)
{
	if(node != nullptr){
		postOrderBST(node->left_child);
		postOrderBST(node->right_child);
		cout <<"\t\t\t" << node->key << endl;
	}

	return;
}

void BSTree::postOrderBST()
{
	postOrderBST(root);

	return;
}

BSTree *createBSTree()
{
	BSTree *node;

	try{
		node = new BSTree();
	}
	catch(bad_alloc){
		cout << "\n\n\t\t\tYikes! Out of memory!!!\n";
		node = nullptr;
	}

	return node;
}

int BSTree::sumBST()
{
	sumBST(root);
	return sum;
}

void BSTree::sumBST(BSTnode *node)
{
	if(node != nullptr)
	{
		sum += node->key;
		sumBST(node->left_child);
		sumBST(node->right_child);
	}
}