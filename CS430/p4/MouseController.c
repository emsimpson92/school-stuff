#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/interrupt.h>
#include <linux/proc_fs.h>
#include <linux/sched.h>
#include <linux/keyboard.h>
#include <asm/uaccess.h>
#include <asm/io.h>
#include <linux/slab.h>
#include <linux/module.h>
#include <linux/init.h>
#include <linux/input.h>
#include <linux/proc_fs.h>

struct input_dev *mouse;
struct notifier_block nb;

int kb_notifier_fn(struct notifier_block *nb, unsigned long action, void* data){
	struct keyboard_notifier_param *kp = (struct keyboard_notifier_param*)data;
	printk("Key:  %d  Lights:  %d  Shiftmax:  %x\n", kp->value, kp->ledstate, kp->shift);
	
    //UP: 103
    //DOWN: 108
    //LEFT: 105
    //RIGHT: 106
    //PAUSE: 119

    switch(kp->value)
    {
        case 103:
            input_report_rel(mouse, REL_Y, -10);
            break;

        case 108:
            input_report_rel(mouse, REL_Y, 10);
            break;

        case 105:
            input_report_rel(mouse, REL_X, -10);
            break;

        case 106:
            input_report_rel(mouse, REL_X, 10);
            break;

        case 119:
            printk("MOUSE CLICKED");
            // For some reason clicking the mouse isn't enough
            input_report_key(mouse, BTN_LEFT, 1);
            input_sync(mouse);
            // Need to include the release of the button for it to work
            input_report_key(mouse, BTN_LEFT, 0);
            // Computers are stupid
            break;
    }
    
	input_sync(mouse);
	return 0;
}

static int __init mm_init(void)
{
	mouse = input_allocate_device();

	mouse->name = "mouse";
	set_bit(EV_REL, mouse->evbit);
	set_bit(REL_X, mouse->relbit);
	set_bit(REL_Y, mouse->relbit);

	set_bit(EV_KEY, mouse->evbit);
	set_bit(BTN_LEFT, mouse->keybit);
	
	input_register_device(mouse);
	nb.notifier_call = kb_notifier_fn;
	register_keyboard_notifier(&nb);
	return 0;
}

static void __exit mm_remove(void)
{
	input_unregister_device(mouse);
	unregister_keyboard_notifier(&nb);
}

MODULE_LICENSE("GPL"); 
module_init(mm_init);
module_exit(mm_remove);