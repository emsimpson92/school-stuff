#include <linux/proc_fs.h>
#include <linux/module.h>
#include <linux/slab.h>
#include <linux/init.h>
#include <linux/tty.h>		/* For fg_console, MAX_NR_CONSOLES */
#include <linux/kd.h>		/* For KDSETLED */
#include <linux/vt.h>
#include <linux/vt_kern.h>
#include <linux/console_struct.h>	/* For vc_cons */

MODULE_LICENSE("GPL");

struct timer_list my_timer;
struct tty_driver *my_driver;
char kbledstatus = 0;

unsigned long blink_delay = HZ/5;
int leds = 7;
#define RESTORE_LEDS  0xFF

ssize_t writeInput(struct file* fp, const char *user_buffer, size_t user_buffer_size, loff_t *offset)
{
	int ledsOn;
	unsigned long delay;
	printk("Write was just called!  User wants to write %s (%lu bytes)\n", user_buffer, user_buffer_size);

    if(user_buffer[0] == 'L')
    {
        ledsOn = (long)user_buffer[1] - '0';
        leds = ledsOn;
    }
    else if(user_buffer[0] == 'D')
    {
        delay = (long)user_buffer[1] - '0';
        blink_delay = HZ/delay;
    }
    else
    {
        printk(KERN_ERR "Invalid user input");
        return -1;
    }
    
    return user_buffer_size;
}

struct file_operations our_file_operations = {
	write: writeInput
}; 

static void my_timer_func(struct timer_list *timers)
{
	int *pstatus = (int *)&kbledstatus;

	if (*pstatus == leds)
		*pstatus = RESTORE_LEDS;
	else
		*pstatus = leds;

	(my_driver->ops->ioctl) (vc_cons[fg_console].d->port.tty, KDSETLED,
			    *pstatus);

	my_timer.expires = jiffies + blink_delay;
	add_timer(&my_timer);
}

static int __init kbleds_init(void)
{
	int i;

	proc_create("leds", 0, 0, &our_file_operations);
	printk(KERN_INFO "kbleds: loading\n");
	printk(KERN_INFO "kbleds: fgconsole is %x\n", fg_console);
	for (i = 0; i < MAX_NR_CONSOLES; i++) {
		if (!vc_cons[i].d)
			break;
		printk(KERN_INFO "poet_atkm: console[%i/%i] #%i, tty %lx\n", i,
		       MAX_NR_CONSOLES, vc_cons[i].d->vc_num,
		       (unsigned long)vc_cons[i].d->port.tty);
	}
	printk(KERN_INFO "kbleds: finished scanning consoles\n");

	my_driver = vc_cons[fg_console].d->port.tty->driver;
	printk(KERN_INFO "kbleds: tty driver magic %x\n", my_driver->magic);

	/*
	 * Set up the LED blink timer the first time
	 */
	
	timer_setup(&my_timer, my_timer_func, 0);
	my_timer.expires = jiffies + blink_delay;
	add_timer(&my_timer);

	(my_driver->ops->ioctl) (vc_cons[fg_console].d->port.tty, KDSETLED, leds);

	return 0;
}

static void __exit kbleds_cleanup(void)
{
	printk(KERN_INFO "kbleds: unloading...\n");
    remove_proc_entry("leds", 0);
	del_timer(&my_timer);
	(my_driver->ops->ioctl) (vc_cons[fg_console].d->port.tty, KDSETLED, RESTORE_LEDS);
}

module_init(kbleds_init);
module_exit(kbleds_cleanup);