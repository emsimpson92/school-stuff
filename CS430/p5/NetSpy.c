#include <linux/module.h>
#include <linux/init.h>
#include <linux/ip.h>
#include <linux/skbuff.h>
#include <linux/netfilter.h>
#include <linux/inet.h>
#include <linux/socket.h>
#include <linux/slab.h>
#include <asm/io.h>
#include <asm/uaccess.h>
#include <linux/sched.h>
#include <linux/proc_fs.h>
#include <linux/kernel.h>
#include <linux/interrupt.h>

#define PROC_FILE_NAME "traffic"

MODULE_LICENSE("GPL");

struct IP_node {
	struct rb_node node;
	unsigned int IP;
	unsigned int numPackets;
};

struct nf_hook_ops net_hook;
struct rb_root root = RB_ROOT;

ssize_t read_simple(struct file *filp,char *buf,size_t count,loff_t *offp ) 
{
	struct IP_node *pos, *n;
	size_t sofar = 0;
	if(*offp)
		return 0;
	//TODO: Stop ignoring compiler warnings
	rbtree_postorder_for_each_entry_safe(pos, n, &root, node){
		sofar += snprintf(buf + sofar, count - sofar, "IP: %pI4\nNumber of packets: %u\n", &(pos->IP), pos->numPackets);
	}
	*offp = sofar;

	return sofar;
}

struct file_operations proc_fops = {
	read: read_simple,
};

unsigned int hook_function(void *priv, struct sk_buff *skb, const struct nf_hook_state *state){
	struct iphdr *ip_header = (struct iphdr *)skb_network_header(skb);
    struct rb_node **new = &(root.rb_node), *parent = NULL;
    struct IP_node *new_node = kmalloc(sizeof(struct IP_node), 1);

	while(*new) {
		struct IP_node *this = container_of(*new, struct IP_node, node);
		parent = *new;
		if(this->IP == ip_header->saddr){
			this->numPackets++;
			kfree(new_node);
			return NF_ACCEPT;
		} else if(this->IP < ip_header->saddr)
			new = &((*new)->rb_right);
		else
			new = &((*new)->rb_left);
	}
    new_node->IP = ip_header->saddr;
    new_node->numPackets = 1;
    rb_link_node(&new_node->node, parent, new);
    rb_insert_color(&new_node->node, &root);

	return NF_ACCEPT;
}

int __init netmon_init(void) {
	net_hook.hook = hook_function;
	net_hook.hooknum = NF_INET_LOCAL_IN;
	net_hook.pf = AF_INET;
	nf_register_net_hook(&init_net, &net_hook);
    proc_create(PROC_FILE_NAME,0,NULL,&proc_fops);
	
    return 0;
}

void __exit netmon_cleanup(void)
{
    struct IP_node *pos, *n;

	nf_unregister_net_hook(&init_net, &net_hook);
    remove_proc_entry(PROC_FILE_NAME,NULL);
    rbtree_postorder_for_each_entry_safe(pos, n, &root, node){
		kfree(pos);
	}
}

module_init(netmon_init);
module_exit(netmon_cleanup);