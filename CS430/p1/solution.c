/* This one is for you to finish!  Have fun! */
#include "tester.h"
#include "stdlib.h"
#include "stdio.h"

int find_best(void* funcs, int caller, size_t count) {
	int highestVal = -1;
	int highestIndex = -1;
    int (**newFuncs)(int) = funcs;
	for(int i = 0; i < count; i++) {
		if(newFuncs[i](caller) > highestVal) {
			highestVal = newFuncs[i](caller);
			highestIndex = i;
		}
	}

	return highestIndex;
}

int* sorted_copy(int foo[], size_t fooLength)
{
	int *fooCopy = malloc(sizeof(int) * fooLength);
	for(int i = 0; i < fooLength; i++)
	{
		fooCopy[i] = foo[i];
	}
	for (int i = 0; i < fooLength; i++)
	{
		for (int j = 0; j < fooLength; j++)
		{
			if (fooCopy[j] > fooCopy[i])
			{
				int tmp = fooCopy[i];
				fooCopy[i] = fooCopy[j];
				fooCopy[j] = tmp;
			}  
		}
	}
	
	return fooCopy;
}

int combine(int foo, int bar)
{
	int foobar = foo | bar;
	printf("%d\n", foobar);
	foobar = foobar >> 3;
	printf("%d\n", foobar);
	foobar = foobar & 0xFFFF00;
	printf("%d\n", foobar);

	return foobar;
}

struct bundle_struct testBundle = {
	find_best,
	sorted_copy,
	combine
};

int main() {
	test(&testBundle);

	return 0;
}
