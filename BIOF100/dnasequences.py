import sys

def complement(dna):
  newDna = []
  for item in dna:
    if item == "A":
      newDna.append("T")
    elif item == "T":
      newDna.append("A")
    elif item == "G":
      newDna.append("C")
    elif item == "C":
      newDna.append("G")
    else:
      newDna.append("N")

  return newDna

def reverseComplement(dna):
  dna.reverse()
  dna = complement(dna)
    
  return dna

try:
  dnaList = sys.argv[1]
except:
  quit()

dna = list(dnaList)

selection = input("1 for reverse, 2 for complement, 3 for reverse complement:\n")
if(selection == "1"):
	dna.reverse()
elif(selection == "2"):
	dna = complement(dna)
else:
	dna = reverseComplement(dna)

print(dna)
